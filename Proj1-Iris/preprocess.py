# you add libraries here too 
# make sure that the methods you write here have required libraries

import pandas as pd
import numpy as np


class PreProcess(object):
	"""
	This class contains various methods to preprocess the data
	before feeding to algorithms defined in methods.py
	
	"""
	# this is like a constructor
	def __init__(self):
		pass




	#############################################################
	######## Specific function for X algorithm ##################
	#############################################################
	def Ymethod(self,dataX,dataY):

		results = ['This will contain processed dataX and dataY']
		return results
	
	



	# this is like a de-constructor
	def __del__(self):
		pass
		